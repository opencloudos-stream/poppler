%global test_sha 03a4b9eb854a06a83c465e82de601796c458bbe9

%bcond_without qt

Summary:       PDF rendering library
Name:          poppler
Version:       23.08.0
Release:       7%{?dist}
License:       (GPLv2 or GPLv3) and GPLv2+ and LGPLv2+ and MIT
URL:           http://poppler.freedesktop.org/
Source0:       http://poppler.freedesktop.org/poppler-%{version}.tar.xz
Source1:       https://gitlab.freedesktop.org/poppler/test/-/archive/%{test_sha}/test-%{test_sha}.tar.gz

Patch0001:     CVE-2024-6239-pdfinfo-Fix-crash-in-broken-documents-when-using-des.patch
Patch0002:     CVE-2024-56378-JBIG2Bitmap-combine-Fix-crash-on-malformed-files.patch
Patch3000:     poppler-0.90.0-position-independent-code.patch
Patch3001:     poppler-21.01.0-glib-introspection.patch

BuildRequires: make cmake gcc-c++ gettext-devel boost-devel
BuildRequires: pkgconfig(cairo) pkgconfig(cairo-ft) pkgconfig(cairo-pdf) pkgconfig(cairo-ps)
BuildRequires: pkgconfig(cairo-svg) pkgconfig(fontconfig) pkgconfig(freetype2)
BuildRequires: pkgconfig(gdk-pixbuf-2.0) pkgconfig(gio-2.0) pkgconfig(gobject-2.0)
BuildRequires: pkgconfig(gobject-introspection-1.0) pkgconfig(gtk+-3.0) pkgconfig(gtk-doc)
BuildRequires: pkgconfig(lcms2) pkgconfig(libjpeg) pkgconfig(libpng) pkgconfig(libtiff-4) pkgconfig(nss)

%if %{with qt}
BuildRequires: pkgconfig(Qt5Core) pkgconfig(Qt5Gui) pkgconfig(Qt5Test) pkgconfig(Qt5Widgets) pkgconfig(Qt5Xml)
%endif

Requires:      poppler-data

%description
This package provides a PDF rendering library.

%package       devel
Summary:       Libraries and headers for poppler
Requires:      %{name} = %{version}-%{release}

%description   devel
This package provides devel files to compile applications based on poppler.

%package       glib
Summary:       Glib wrapper for poppler
Requires:      %{name} = %{version}-%{release}

%description   glib
PDF rendering library.

%package       glib-devel
Summary:       Development files for glib wrapper
Requires:      %{name}-glib = %{version}-%{release}
Requires:      %{name}-devel = %{version}-%{release}
Suggests:      %{name}-doc = %{version}-%{release}

%description   glib-devel
PDF rendering library.

%package       glib-doc
Summary:       Documentation for glib wrapper
BuildArch:     noarch

%description   glib-doc
PDF rendering library.

%if %{with qt}
%package       qt5
Summary:       Qt5 wrapper for poppler
Requires:      %{name} = %{version}-%{release}

%description   qt5
PDF rendering library.

%package       qt5-devel
Summary:       Development files for Qt5 wrapper
Requires:      %{name}-qt5 = %{version}-%{release}
Requires:      %{name}-devel = %{version}-%{release}
Requires:      qt5-qtbase-devel

%description qt5-devel
PDF rendering library.
%endif

%package       cpp
Summary:       Pure C++ wrapper for poppler
Requires:      %{name} = %{version}-%{release}

%description   cpp
PDF rendering library.

%package       cpp-devel
Summary:       Development files for C++ wrapper
Requires:      %{name}-cpp = %{version}-%{release}
Requires:      %{name}-devel = %{version}-%{release}

%description   cpp-devel
PDF rendering library.

%package       utils
Summary:       Command line utilities for converting PDF files
Requires:      %{name} = %{version}-%{release}
%description   utils
Command line tools for manipulating PDF files and converting them to
other formats.

%prep
%autosetup -p1 -b 1
chmod -x poppler/CairoFontEngine.cc

%build
%cmake \
  -DENABLE_CMS=lcms2 \
  -DENABLE_DCTDECODER=libjpeg \
  -DENABLE_GTK_DOC=ON \
  -DENABLE_LIBOPENJPEG=none \
%if ! %{with qt}
  -DENABLE_QT5=OFF \
%endif
  -DENABLE_QT6=OFF \
  -DENABLE_UNSTABLE_API_ABI_HEADERS=ON \
  -DENABLE_ZLIB=OFF \
  ..
%cmake_build

%install
%cmake_install

%check
%make_build test

export PKG_CONFIG_PATH=%{buildroot}%{_datadir}/pkgconfig:%{buildroot}%{_libdir}/pkgconfig
test "$(pkg-config --modversion poppler)" = "%{version}"
test "$(pkg-config --modversion poppler-cpp)" = "%{version}"
test "$(pkg-config --modversion poppler-glib)" = "%{version}"

%if %{with qt}
test "$(pkg-config --modversion poppler-qt5)" = "%{version}"
%endif

%files
%license COPYING
%doc README.md
%{_libdir}/libpoppler.so.*

%files devel
%{_libdir}/pkgconfig/poppler.pc
%{_libdir}/libpoppler.so
%dir %{_includedir}/poppler/
%{_includedir}/poppler/*.h
%{_includedir}/poppler/fofi/
%{_includedir}/poppler/goo/
%{_includedir}/poppler/splash/

%files glib
%{_libdir}/libpoppler-glib.so.8*
%{_libdir}/girepository-1.0/Poppler-0.18.typelib

%files glib-devel
%{_libdir}/pkgconfig/poppler-glib.pc
%{_libdir}/libpoppler-glib.so
%{_datadir}/gir-1.0/Poppler-0.18.gir
%{_includedir}/poppler/glib/

%files glib-doc
%license COPYING
%{_datadir}/gtk-doc/

%if %{with qt}
%files qt5
%{_libdir}/libpoppler-qt5.so.1*

%files qt5-devel
%{_libdir}/libpoppler-qt5.so
%{_libdir}/pkgconfig/poppler-qt5.pc
%{_includedir}/poppler/qt5/
%endif

%files cpp
%{_libdir}/libpoppler-cpp.so.0*

%files cpp-devel
%{_libdir}/pkgconfig/poppler-cpp.pc
%{_libdir}/libpoppler-cpp.so
%{_includedir}/poppler/cpp

%files utils
%{_bindir}/pdf*
%{_mandir}/man1/*

%changelog
* Mon Dec 23 2024 Shuo Wang <abushwang@tencent.com> - 23.08.0-7
- [Type] security
- [DESC] fix CVE-2024-56378
- JBIG2Bitmap::combine: Fix crash on malformed files

* Fri Nov 01 2024 Rebuild Robot <rebot@opencloudos.org> - 23.08.0-6
- [Type] bugfix
- [DESC] Rebuilt for nss

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 23.08.0-5
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 23.08.0-4
- Rebuilt for loongarch release

* Mon Jun 24 2024 Shuo Wang <abushwang@tencent.com> - 23.08.0-3
- fix CVE-2024-6239 and backport its pre-patch
- pdfinfo: Fix crash in broken documents when using -dests

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 23.08.0-2
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Aug 21 2023 Shuo Wang <abushwang@tencent.com> - 23.08.0-1
- update to 23.08.0

* Wed Aug 02 2023 kianli <kianli@tencent.com> - 22.08.0-5
- Rebuilt for libtiff 4.5.1

* Tue Aug 01 2023 rockerzhu rockerzhu@tencent.com - 22.08.0-4
- Rebuilt for boost 1.82.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 22.08.0-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 22.08.0-2
- Rebuilt for OpenCloudOS Stream 23

* Tue Sep 13 2022 Shuo Wang <abushwang@tencent.com> - 22.08.0-1
- initial build

